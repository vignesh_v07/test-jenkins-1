Public Class batchtry_ctrlr 
{
    public String attach{get;set;}
    public string selectedvalue{get;set;}       
    public string filename{get;set;}
    public Blob contentFile{get;set;}
    public list<string> sr{get;set;}
    String[] filelines = new String[]{};
    Database.SaveResult[] srList;
    public string batchProcessId;
    public string IdOfBatch;

    public List<case> tobeuploaded=new List<case>();
    
    public Document doc {get;set;}
    //Link for case upload sheet format.
    public PageReference getFile() 
    {
        String fileId = System.currentPageReference().getParameters().get('id'); 
        if(fileId != null) 
        {
          doc = [SELECT Id, Name, Description, ContentType, Type, Url, BodyLength, Body 
                    FROM Document WHERE Name = 'Upload_Template'];
        }
        return null;            
    }
    
    //Select Object Drop_Down.
    public List<SelectOption> getItem() 
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Case','Case'));
        return options;
    }

    //LOGIC TO IMPORT CASE AND CASE COMMENT.
    public pagereference submit1()
    {
    Integer o=0;
    Integer c=0;
    Integer c1=0;
    Integer c2=0;
    sr=new list<String>(); 
    String allerr;
    List<String> partialerr=new List<String>(); 
    
    if(filename!=null)
    {   
        Integer indexvalue=filename.lastindexof('.');      
      
      if(filename.substring((indexvalue+1),(indexvalue+4)).equalsignorecase('csv'))
      {
            attach=contentFile.toString();  
            filelines = attach.split('\n');  
            String caseno;
            String parentcaseno;
            String[] inputvalues = new String[]{};
            List<string> CaseNoFromExcel=new List<string>();
            List<string> OwnerNameFromExcel=new List<string>();
            String casenum;
            String parentCaseNum;
            system.debug('FILE LINES!!!!!!!!!!!!!!!'+filelines);
            system.debug('!!!!!!!!!!!!'+filelines.size());
   
        //For loop to get case numbers from excel file
        for (Integer q=1;q<filelines.size();q++)
         {
            inputvalues = filelines[q].split(',');
           
            // For child case number
            if(inputvalues[0].length()==4)
            {
              casenum='0000'+inputvalues[0];
              CaseNoFromExcel.add(casenum);
            }
            else if(inputvalues[0].length()==5)
            {
              casenum='000'+inputvalues[0];
              CaseNoFromExcel.add(casenum);
            } 
            else if(inputvalues[0].length()==6)
            {
               casenum='00'+inputvalues[0];
               CaseNoFromExcel.add(casenum);
            } 
            else if(inputvalues[0].length()==7)
            {
               casenum='0'+inputvalues[0];
               CaseNoFromExcel.add(casenum);
            } 
            else 
            {
               CaseNoFromExcel.add(inputvalues[0]);
            }
            //For parent case number  
                if(inputvalues[1].length()!=0)
                {  
                  if(inputvalues[1].length()==4)
                  {
                  parentCaseNum='0000'+inputvalues[1];
                  CaseNoFromExcel.add(parentCaseNum);
                  }
                  else if(inputvalues[1].length()==5)
                  {
                    parentCaseNum='000'+inputvalues[1];   
                    CaseNoFromExcel.add(parentCaseNum);
                  }
                  else if(inputvalues[1].length()==6)
                  {
                   parentCaseNum='00'+inputvalues[1];
                   CaseNoFromExcel.add(casenum);
                  } 
                  else if(inputvalues[1].length()==7)
                  {
                   parentCaseNum='0'+inputvalues[1];
                   CaseNoFromExcel.add(casenum);
                  } 
                  else 
                  {
                   CaseNoFromExcel.add(inputvalues[1]);
                  }
                 } 
         }
        system.debug('!!!!!!CASE NO FROM XL!!!!!!!!!!!!!'+CaseNoFromExcel.size());
         //For loop to get owner names from excel file
    /*     for (Integer e=3;e<filelines.size();e++)
         {
            inputvalues = filelines[e].split(',');
            OwnerNameFromExcel.add(inputvalues[7]);
         }
        List<Group> G1=[select id, name from Group where name in:OwnerNameFromExcel]; 
        */
        List<Case> list1=[select casenumber,id from case where casenumber in:CaseNoFromExcel];
 
        //For loop for all the updations.
        for (Integer r=1;r<filelines.size();r++)
         {
              inputvalues = filelines[r].split(',');
              //For case nos.
              if(inputvalues[0].length()==4)
              {
              caseno='0000'+inputvalues[0];
              }
              else if(inputvalues[0].length()==5)
              {
              caseno='000'+inputvalues[0];
              }
              else if(inputvalues[0].length()==6)
              {
              caseno='00'+inputvalues[0];
              }
              else if(inputvalues[0].length()==7)
              {
              caseno='0'+inputvalues[0];
              }
              else
              {
              caseno = String.valueOf(inputvalues[0]);
              }
              //For parentcase nos.
              if(inputvalues[1].length()==4)
              {
              caseno='0000'+inputvalues[1];
              }
              else if(inputvalues[1].length()==5)
              {
              parentcaseno='000'+inputvalues[1];
              }
              else if(inputvalues[1].length()==6)
              {
              parentcaseno='00'+inputvalues[1];
              }
              else if(inputvalues[1].length()==7)
              {
              parentcaseno='0'+inputvalues[1];
              }
              else
              {
              parentcaseno= String.valueOf(inputvalues[1]);
              }
           
           system.debug('Case no & parent case no!!!!!!!!!!!!!'+caseno+' '+parentcaseno);   
           for(case obj:list1)
           {
              if(obj.casenumber.equals(caseno))
               {
                  obj.status=inputvalues[2];
                  if(inputvalues[3]!=null)
                  {
                  obj.priority =inputvalues[3];
                  }
                  obj.subject=inputvalues[4];
                  obj.TestField__c=inputvalues[6];
                  
                  
              /*
                  for(Group obj1:G1)
                  {
                   if(obj1.name.equals(inputvalues[7]))
                   { 
                      obj.ownerId=obj1.id;
                       break;
                   }
                  }
             */
             system.debug('!!!!!!!!!PARENT!!!!!'+(parentcaseno.length()!=0));
                  if(parentcaseno.length()!=0 && parentcaseno!=null && parentcaseno!='')
                  {
                      for(Case obj2:list1)
                      {   
                          if(obj2.casenumber.equals(parentcaseno))
                          {
                              obj.parentId=obj2.Id;
                              break;           
                          }
                      }            
                   }
                   else
                   {
                      obj.parentId=NULL;
                   } 
               
              CaseComment CC;        
              CC=new CaseComment();
              if(inputvalues[5]!=null && inputvalues[5]!='')
              {
              CC.CommentBody=inputvalues[5];
              CC.ParentID=obj.id;
              insert CC;
              }
               tobeuploaded.add(obj);
               break;
              }
           }
         }
 system.debug('!!!!!!!!!!!!!!!!tobeuploaded'+tobeuploaded.size());
 
  //calling the Batch class......
            BatchUpload mu=new BatchUpload(tobeuploaded);
         //Parameters of ExecuteBatch(context,BatchSize)         
            batchProcessId=database.executebatch(mu,5);
            system.debug('!!!!!!!!'+batchProcessId);
            IdOfBatch=(string)batchProcessId;
            IdOfBatch=IdOfBatch.substring(0,15);
            system.debug('BATCH ID!!!!!!!!!!!!'+IdOfBatch);
            system.debug('AFTER DATABASE.EXECUTEBATCH!!!!!!!!!!!!!!!!!');
            system.debug('!!!!!!!VALUE OF UPDATERESULT!!!!!!!!!!'+BatchUpdate_info.Updateresult);
            
            if(BatchUpdate_info.Updateresult == true)
            {
            system.debug('!!!!!!!!!!BATCH PROCESS COMPLETED!!!!!');  
            ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,'Process Completed!!');  
            ApexPages.addMessage(mess);
            BatchUpdate_info.Updateresult = false;
       //     Pagereference pageref=new Pagereference('/apex/testpageformsg');
       //     return pageref;
            } 
 /*
     try
     {
     system.debug('Entered try!!!!!!!!!!!!');
       srList= Database.update(tobeuploaded,false);
       system.debug('srList.size()!!!!!!!!!!'+srList.size());
         for (Database.SaveResult sr : srList) 
          { 
             if (sr.isSuccess()) 
             {   
                 o++; 
                 c=1;                     
             }
             else 
             {
             // Operation failed, getting all the errors.
               for(Database.Error err : sr.getErrors()) 
               {
               if(o==0)
               {
               c1=1;
               allerr=err.getMessage();
               }
               else if(o!=0 && (o!=srList.size()))
               {
               c2=1;
               partialerr.add(err.getMessage());
               }
                
               }
             }             
           }
       }
       catch(exception e)
       {
           ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Updation Unsuccessful!!');   
           ApexPages.addMessage(errormsg); 
       }
     
     if(c==1)
     {
      ApexPages.Message successmsg = new ApexPages.Message(ApexPages.severity.INFO,'Totally '+o+' records were uploaded and updated successfully.');
      ApexPages.addMessage(successmsg);
      ApexPages.Message successmsg1 = new ApexPages.Message(ApexPages.severity.INFO,'Please refresh the page for subsequent updates.');
      ApexPages.addMessage(successmsg1);  
     }
     if(c1==1)
     {
      ApexPages.Message errmsg= new ApexPages.Message(ApexPages.severity.ERROR,'The following error has occured on the records. Error Message:'+allerr);   
      ApexPages.addMessage(errmsg);
     }
     if(c2==1)
     {
      ApexPages.Message errmsg= new ApexPages.Message(ApexPages.severity.ERROR,'The following error has occured. Error Message:'+partialerr);   
      ApexPages.addMessage(errmsg);
     }
     */
      }
      else
      {
           ApexPages.Message errormsg2 = new ApexPages.Message(ApexPages.severity.ERROR,'The file you selected is not a .csv type.');  
           ApexPages.addMessage(errormsg2);
      }
     }
     else
     {
           ApexPages.Message errormsg3 = new ApexPages.Message(ApexPages.severity.ERROR,'select a file to upload.');  
           ApexPages.addMessage(errormsg3);
     }
     return null;
   } 
}
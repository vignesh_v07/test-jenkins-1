public with sharing class SpeechRecognizationController {

@RemoteAction
public static string checkAvailability(string passedValue)
{
    List<string> splitVa = new List<String>();
    //If am in a detail page of a record..
    String recordId=ApexPages.currentPage().getParameters().get('id');
    if(recordId != null)
    {
        system.debug('Yes!! We are in detail page of a record with speech to text still in context!!!'); 
        if(passedValue!=null)
        {  
            //this is for adding note to a record. speech pattern - 'add note this is a team work', where body of the note is 'this is a team work'
            splitVa = passedValue.split(' ');
            string bodyContent;   
            if((splitVa[0].toLowerCase()+''+splitVa[1].toLowerCase())=='add note')
            {
                Integer i=0;
                Integer j=0;
                Integer listSize = splitVa.size();
                while(i==1)
                {
                    // removing first two values - 'add note'
                    splitVa.remove(i);
                    i++;
                    //removing last two values - 'stop note'
                    splitVa.remove(listsize-i);
                }
                //now the list will have just the body content.
                for(String bodyC:splitVa)
                {
                    if(j==0)
                    { bodyContent = bodyC; }
                    else
                    { bodyContent = bodyContent +''+bodyC; }
                    j++;                
                }
                Note note_attach = new Note();
                note_attach.Title = 'New Note_'+Date.Today();
                note_attach.parentId = recordId;
                note_attach.body = bodyContent;
                insert note_attach;
            }   
        }
        return '';
    }
    else
    {
       if(passedValue!=null)
       {
          splitVa = passedValue.split(' ');
          if(splitVa[0].toLowerCase() != null)  //.contains('account')
          {
             Sobject obj;
             Schema.SObjectType targetType = Schema.getGlobalDescribe().get(splitVa[0].toLowerCase());
             obj = targetType.newSObject();
             string s = 'select id from '+obj+'  where name like \'%'+splitVa[1]+'%\' limit 1';
             system.debug(s);
              List<Account> accountList = Database.query(s);
              if(accountList!=null && accountList.size()>0)
                 return accountList[0].id;
          }
       }
      return '';
    }
}
//redirect to page
@RemoteAction
public static PageReference redirect(string passedValue)
{
    PageReference pg = null;
    pg =  new PageReference('/passedValue');
    pg.setRedirect(true);
    return pg;
}
}
























/*public with sharing class SpeechRecognizationController {

@RemoteAction
public static string checkAvailability(string passedValue)
{
 
   if(passedValue!=null)
   {
      List<string> splitVa = passedValue.split(' ');
      if(splitVa[0].toLowerCase() != null)  //.contains('account')
      {
         Sobject obj;
         Schema.SObjectType targetType = Schema.getGlobalDescribe().get(splitVa[0].toLowerCase());
         obj = targetType.newSObject();
         string s = 'select id from '+obj+'  where name like \'%'+splitVa[1]+'%\' limit 1';
         system.debug(s);
          List<Account> accountList = Database.query(s);
          if(accountList!=null && accountList.size()>0)
             return accountList[0].id;
      }
   }

  return '';
}

//redirect to page
@RemoteAction
public static PageReference redirect(string passedValue)
{
  PageReference pg = null;
 pg =  new PageReference('/passedValue');
 pg.setRedirect(true);
  return pg;
}


}*/
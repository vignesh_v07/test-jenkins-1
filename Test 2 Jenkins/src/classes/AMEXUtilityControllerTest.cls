// Class           : AMEXUtilityControllerTest
// Description     : Test class for AMEXUtilityController
// Author          : V. Mittal
// Created Date    : Sep 09, 2011
//-------------------------------------------------------------------------------
// Modified Date        Modified By         Description
//-------------------------------------------------------------------------------
// 
//-------------------------------------------------------------------------------
@isTest
private class AMEXUtilityControllerTest{
    static testmethod void runTests() {
        RecordType rt = [select Id, Name, SobjectType from RecordType LIMIT 1];
        String rtId = AMEXUtilityController.getRecordTypeId(rt.Name, rt.SobjectType);
        String rtName = AMEXUtilityController.getRecordTypeName(rt.Id);
        system.assertEquals(rt.Id, rtId);
        system.assertEquals(rt.Name, rtName);
        
        String rtId1 = AMEXUtilityController.getRecordTypeId('AAAAAAA', 'XXXXXXX');
        String rtName1 = AMEXUtilityController.getRecordTypeName('00100009990090J');
        system.assertEquals(null, rtId1);
        system.assertEquals(null, rtName1);
    }
}
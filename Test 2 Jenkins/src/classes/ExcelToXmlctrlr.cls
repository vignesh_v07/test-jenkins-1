public class ExcelToXmlctrlr
{

    public String textToParse {get;set;}
    public String parsedText {get;set;}
    public String filename {get;set;}
    public Blob Contentfile {get;set;}
    public String outxmlstring {get;set;}
    public List<String> childs =new List<String>();
    public Integer c=0;
    public List<Account> listaccounts = new List<Account> ();
    public Database.SaveResult[] srList;
    public list<logfiledownload> finalloglist{get;set;}
    public string errors='';
    
    public PageReference parse() {
      //  parse1(textToParse);
      DOM.Document xmlDOC = new DOM.Document();
      xmlDOC.load(Contentfile.toString());
      //DOM.XMLNode rootElement = xmlDOC.getRootElement(); -> To get the Root Element..
      //outxmlstring=String.valueof(xmlDOC.getRootElement().getName()); -> To get the Name of the Root Element..
      for(DOM.XMLNode xmlnodeobj:xmlDOC.getRootElement().getChildElements())
       //.getChildren())
       {  //child elements.....        
          listaccounts=loadChilds(xmlnodeobj);         
       }  
       system.debug('!!!!!CHILDS!!!!'+listaccounts.size());
       system.debug('!!!!!CHILDS!!!!'+listaccounts);
       finalloglist=new list<logfiledownload>();
       try
       {
       srList= Database.insert(listaccounts,false);
             
         for (Database.SaveResult sr : srList) 
          { 
             c++;
             logfiledownload wlf=new logfiledownload();      
             wlf.a=c;
             wlf.recordId=sr.getId();
             // Operation was successful, so get the ID of the record that was processed
             if (sr.isSuccess()) 
             {  
                 wlf.recordName=listaccounts[c-1].Name;
                 wlf.result='Success';
             }
             else
             {
             errors='';  
             // Operation failed, getting all the errors.
              for(Database.Error err : sr.getErrors()) 
               {
                  errors += err.getMessage()+'\n';
                  wlf.recordName=listaccounts[c-1].Name;
               }          
               wlf.result='Failed';
               wlf.b=errors;               
            }
            finalLogList.add(wlf);            
           } 
           system.debug('Final log list!!!!!!!!!'+finalLogList);
       }
       catch(exception e)
       {
           ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Updation Unsuccessful!!. Please Refresh the page for further uploading.');   
           ApexPages.addMessage(errormsg); 
       } 
                 
      //  readResponse(Contentfile.toString());
        ApexPages.Message successmsg = new ApexPages.Message(ApexPages.severity.INFO,'!!!!Success!!!!');
        ApexPages.addMessage(successmsg);
        return null;
    }

    private String parse1(String toParse) 
    {  
      DOM.Document doc = new DOM.Document();        
      try {  
      doc.load(toParse);      
       DOM.XMLNode root = doc.getRootElement(); 
       parsedText= walkThrough(root);
       return walkThrough(root);  
       } catch (System.XMLException e) {  // invalid XML  
       return e.getMessage();  
       }  
    }
    
    private String walkThrough(DOM.XMLNode node) 
    {  
      String result = '\n';  
       if (node.getNodeType() == DOM.XMLNodeType.COMMENT) {  
       return 'Comment (' +  node.getText() + ')';  
       }  
       if (node.getNodeType() == DOM.XMLNodeType.TEXT) {  
       return 'Text (' + node.getText() + ')';  
       }  
       if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {  
       result += 'Element: ' + node.getName();  
       if (node.getText().trim() != '') {  
       result += ', text=' + node.getText().trim();  
       }  
       if (node.getAttributeCount() > 0) {   
       for (Integer i = 0; i< node.getAttributeCount(); i++ ) {  
       result += ', attribute #' + i + ':' + node.getAttributeKeyAt(i) + '=' + node.getAttributeValue(node.getAttributeKeyAt(i), node.getAttributeKeyNsAt(i));  
       }    
       }  
       for (Dom.XMLNode child: node.getChildElements()) {  
       result += walkThrough(child);  
       }  
       return result;  
       }  
      return '';  //should never reach here   
     }    
     
     // New Code........
    public void readResponse(String toBeParsed) {  
    XmlStreamReader reader=new XmlStreamReader(toBeParsed);
        // Account record we will be looping  
        Account accountRecord;   
        // List of accounts to store the value  
        List < Account > accountList = new list < Account > ();   
        system.debug('!!!!!!!!!'+reader.getEventType());
        system.debug('!!!!!!!!!!!'+XmlTag.START_ELEMENT);
        system.debug('!!reader!!'+reader);
        // Is there any element next?  
        while (reader.hasNext()) {    
            // Is the next element an opening tag?  
            system.debug('!!!!!!!!!'+(c++));
            if (reader.getEventType() == XmlTag.START_ELEMENT) {   
                // Check if the first element is Account  
                if ('Name' == reader.getLocalName()) {  
                // if opening tag of account is found initialize AccountRecord  
                accountRecord = new Account();  
                break;
                } else if ('AccountNumber' == reader.getLocalName()) {  
                // If you find any other opening tag, extract the string value  
                accountRecord.AccountNumber= getValueFromTag(reader);  
                break;
                } else if ('CustomerPriority__c' == reader.getLocalName()) {  
                // If you find any other opening tag, extract the string value  
                accountRecord.CustomerPriority__c= getValueFromTag(reader);  
                break;
                } else if ('Phone' == reader.getLocalName()) {  
                // If you find any other opening tag, extract the string value  
                accountRecord.Phone= getValueFromTag(reader);  
                break;
                } else if ('AccountSite' == reader.getLocalName()) {
                    // If you find any other opening tag, extract the string value  
                accountRecord.Site= getValueFromTag(reader);  
                break;
                } 
            }else if (reader.getEventType() == XmlTag.END_ELEMENT) {  
                // Is the next element an end tag? If yes is it an Account or an Accounts tag?  
                if ('Name' == reader.getLocalName()) {  
                // If you find end tag called account, push the account record in list  
                accountList.add(accountRecord);  
                break;
                } else if ('AccountNumber' == reader.getLocalName()) {  
                // We have reached end of file, just exit  
                accountList.add(accountRecord);
                break;  
                }  
                else if ('CustomerPriority__c' == reader.getLocalName()) {  
                // We have reached end of file, just exit 
                accountList.add(accountRecord); 
                break;  
                }
                else if ('Phone' == reader.getLocalName()) {  
                // We have reached end of file, just exit  
                accountList.add(accountRecord);
                break;  
                }
                else if ('AccountSite' == reader.getLocalName()) {  
                // We have reached end of file, just exit  
                accountList.add(accountRecord);
                break;  
                }                
            }  
        }  
        ApexPages.Message successmsg1 = new ApexPages.Message(ApexPages.severity.INFO,accountList.size()+' records are to be inserted.!');
        ApexPages.addMessage(successmsg1);

        system.debug('Total records to be inserted!!!!!!!!'+accountList.size());
    }  
    // This is an extra function to read data between opening and closing tag.   
    // It will return the string of value from between tags  
    public string getValueFromTag(XMLStreamReader reader) {  
    String DataValue;  
    while (reader.hasNext()) {  
    if (reader.getEventType() == XmlTag.END_ELEMENT) {  
    break;  
    } else if (reader.getEventType() == XmlTag.CHARACTERS) {  
    DataValue = reader.getText();  
    }  
    reader.next();  
    }  
    return DataValue;  
    }
    
    // Another method to parse using DOM!!!     
    //loading the child elements
    public List<Account> loadChilds(DOM.XMLNode xmlnode)
    {   
        //Instantiate objects to insert records.. 
        Account ref=new Account();      
        
        for(Dom.XMLNode child : xmlnode.getChildElements()) // Or xmlnode.getChildren()
        {
          childs.clear();          
          if(child.getText()!= null)
          {
         // outxmlstring+='\n'+child.getName()+': '+child.getText(); 
              if(child.getName()=='Name')
              {
                  ref.Name=child.getText();
              }
              if(child.getName()=='AccountNumber')
              {
                  ref.AccountNumber=child.getText();
              }
              if(child.getName()=='CustomerPriority__c')
              {
                  ref.CustomerPriority__c=child.getText();
              }
              if(child.getName()=='Phone')
              {
                  ref.Phone=child.getText();
              }
              if(child.getName()=='AccountSite')
              {
                  ref.Site=child.getText();
              }                                              
          }    
                                    
        //  system.debug('childs value!!!!'+childs);
        //  loadChilds(child);   
        } 
           listAccounts.add(ref);  
        return listAccounts;
    }   
    // Inner Class
     //class-2  
    public class logfiledownload
    {
    public Integer a{get;set;}
    public String b{get;set;}
    public string result{get;set;}
    public String recordId{get;set;}
    public String recordName{get;set;} 
    public String merNumber{get;set;}
    public String errorrecords{get;set;}
    }  
}
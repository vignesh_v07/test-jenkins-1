//Class used by other test classes. This class will retrieve csv files stored in docs and which will be used
//by test classes to cover the requirement scenarios..
public class TestClassGenerator
{
    public Document docs;
    public TestClassGenerator(String category){
    
        if(category == 'obj')
        {
            docs = [select id, name, body from Document where name =: 'Sobject Upload File' limit 1];    
            system.debug('!!!'+docs);
        }
    }
}
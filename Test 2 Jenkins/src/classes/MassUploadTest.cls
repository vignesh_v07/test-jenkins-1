@isTest  
private class MassUploadTest
{  
    public static testMethod void validatetest() 
    {
        Test.startTest();
        GenericImportController gic = new GenericImportController();
        gic.obj = 'Account';
        gic.selectaction = 'Insert';
        gic.csvAsString = 'testFile.csv';
        gic.getSelectActions();
        gic.getObjectName();
        gic.getObjectFields();
        
        String testBlob = 'Account Name'+','+'Account Number'+','+'\n';
        testBlob += 'Account1'+','+'123'+'\n';
        testBlob += 'Account2'+','+'123'+'\n';    
        testBlob += 'Account3'+','+'123'+'\n';
        testBlob += 'Account4'+','+'123'+'\n';
        gic.csvFileBody=Blob.valueOf(testBlob);
        gic.readcsvFile();
        Test.stopTest();       
    }    
}
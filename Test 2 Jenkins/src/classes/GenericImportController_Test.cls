@isTest  
private class GenericImportController_Test
{  
    public static testMethod void validatetest() 
    {
        List<String> accnames = new List<String>();
        Test.startTest();
        GenericImportController gic = new GenericImportController();
        gic.obj = 'Account';
        gic.selectaction = 'Insert';
        gic.csvAsString = 'testFile.csv';
        gic.getSelectActions();
        gic.getObjectName();
        gic.getObjectFields();
        
        //Partial Insert..
        String testBlob = 'Account Name'+','+'Account Number'+','+'Employees'+'\n';
        testBlob += 'VAccount1'+','+'123'+','+'2'+'\n';
        testBlob += 'VAccount2'+','+'123'+','+'2'+'\n';    
        testBlob += ''+','+'123'+','+'2'+'\n';
        testBlob += 'VAccount4'+','+'123'+','+'2'+'\n';
        gic.csvFileBody=Blob.valueOf(testBlob);
        gic.readcsvFile();
        
        accnames.add('VAccount1');accnames.add('VAccount2');accnames.add('VAccount4');
        
        List<Account> insertedAccs = [select id,name from Account where name in : accnames limit 3];
        system.assertEquals(3, insertedAccs.size());
        
        GenericImportController gic2 = new GenericImportController();
        gic2.obj = 'Account';
        gic2.selectaction = 'Update';
        gic2.csvAsString = 'testFile.csv';
        //Partial Update
        String testBlob2 = 'Account Name'+','+'Account Number'+','+'Id'+','+'Employees'+'\n';
        testBlob2 += 'VAccount1'+','+'42111'+','+insertedAccs[0].id+','+'asd'+'\n';
        testBlob2 += 'VAccount2'+','+'24212'+','+insertedAccs[1].id+','+'1'+'\n';    
        testBlob2 += ''+','+'123'+','+'2'+'\n';
        testBlob2 += 'VAccount4'+','+'6723'+','+insertedAccs[2].id+','+'1'+'\n';
        gic2.csvFileBody=Blob.valueOf(testBlob2);
        gic2.readcsvFile();
        
        GenericImportController gic3 = new GenericImportController();
        gic3.obj = 'Account';
        gic3.selectaction = 'Insert';
        gic3.csvAsString = 'testFile.csv';
        //Successful Insertion
        String testBlob3 = 'Account Name'+','+'Account Number'+','+'Employees'+'\n';
        testBlob3 += 'VAccount1'+','+'123'+','+'2'+'\n';
        testBlob3 += 'VAccount2'+','+'123'+','+'2'+'\n';    
        testBlob3 += 'VAccount4'+','+'123'+','+'2'+'\n';
        gic3.csvFileBody=Blob.valueOf(testBlob3);        
        gic3.readcsvFile();
        
        List<Account> insertedAccs3 = [select id,name from Account where name in : accnames limit 3];
        system.assertEquals(3, insertedAccs3.size());
        
        GenericImportController gic4 = new GenericImportController();
        gic4.obj='Account';
        gic4.selectaction = 'Update';
        gic4.csvAsString = 'testFile.csv';
        //Successful Updation
        String testBlob6 = 'Account Name'+','+'Account Number'+','+'Id'+','+'Employees'+'\n';
        testBlob6 += 'VAccount1'+','+'42111'+','+insertedAccs3[0].id+','+'1'+'\n';
        testBlob6 += 'VAccount2'+','+'24212'+','+insertedAccs3[1].id+','+'1'+'\n';    
        testBlob6 += 'VAccount4'+','+'6723'+','+insertedAccs3[2].id+','+'1'+'\n';
        gic4.csvFileBody=Blob.valueOf(testBlob6);
        gic4.readcsvFile();
        
        GenericImportController gic5 = new GenericImportController();
        gic5.obj = 'Account';
        gic5.selectaction = 'Insert';
        gic5.csvAsString = 'testFile.csv';
        //Insertion Failed
        String testBlob4 = 'Account Name'+','+'Account Number'+','+'Employees'+'\n';
        testBlob4 += ''+','+'123'+','+'2'+'\n';
        testBlob4 += ''+','+'123'+','+'2'+'\n';    
        testBlob4 += ''+','+'123'+','+'2'+'\n';
        gic5.csvFileBody=Blob.valueOf(testBlob4);        
        gic5.readcsvFile();
        
        GenericImportController gic7 = new GenericImportController();
        gic7.obj='Account';
        gic7.selectaction = 'Update';
        gic7.csvAsString = 'testFile.csv';
        //Updation Failed
        String testBlobA = 'Account Name'+','+'Account Number'+','+'Id'+','+'Employees'+'\n';
        testBlobA += ''+','+'42111'+','+insertedAccs3[0].id+','+'1'+'\n';
        testBlobA += ''+','+'24212'+','+insertedAccs3[1].id+','+'1'+'\n';  
        testBlobA += ''+','+'6723'+','+insertedAccs3[2].id+','+'1'+'\n';
        gic7.csvFileBody=Blob.valueOf(testBlobA);
        gic7.readcsvFile();
                
        //No Record to insert/update/delete msg scenario..
        GenericImportController gic8 = new GenericImportController();
        gic8.obj='Account';
        gic8.selectaction = 'Insert';
        gic8.csvAsString = 'testFile.csv';
        //Updation Failed
        String testBlobX = 'Account Name'+','+'Account Number'+','+'Employees'+'\n';
        gic8.csvFileBody=Blob.valueOf(testBlobX);
        gic8.readcsvFile();
        ApexPages.Message[] Msg1= ApexPages.getMessages();
        System.assertEquals('No records to insert/update/delete!',Msg1[5].getDetail());
        
        //Covering 'else' errors..
        GenericImportController gic9 = new GenericImportController();
        gic9.readcsvFile();
        ApexPages.Message[] Msg2= ApexPages.getMessages();
        System.assertEquals('Please select a object!',Msg2[6].getDetail());
        
        gic9.obj = 'Account';
        gic9.readcsvFile();
        ApexPages.Message[] Msg3= ApexPages.getMessages();
        System.assertEquals('Please select an action!',Msg3[7].getDetail()); 
        
        gic9.obj = 'Account';     
        gic9.selectaction = 'Insert';
        gic9.readcsvFile();
        ApexPages.Message[] Msg4= ApexPages.getMessages();
        System.assertEquals('Please select a file to upload!',Msg4[8].getDetail());
        
        gic9.obj = 'Account';     
        gic9.selectaction = 'Insert';
        gic9.csvAsString = 'testFile.doc';
        gic9.readcsvFile();
        ApexPages.Message[] Msg5= ApexPages.getMessages();
        System.assertEquals('The file should be of CSV type!',Msg5[9].getDetail());
        
        //For deletion...
        GenericImportController gic10 = new GenericImportController();
        gic10.obj='Account';
        gic10.selectaction = 'Insert';
        gic10.csvAsString = 'testFile.csv';        
        String testBlobD = 'Account Name'+','+'Account Number'+','+'Employees'+'\n';
        testBlobD += 'DeleteAccount'+','+'42111'+','+'1'+'\n';
        testBlobD += 'DeleteAccount1'+','+'42111'+','+'1'+'\n';
        testBlobD += 'DeleteAccount2'+','+'42111'+','+'1'+'\n';
        testBlobD += 'DeleteAccount3'+','+'42111'+','+'1'+'\n';
        gic10.csvFileBody=Blob.valueOf(testBlobD);
        gic10.readcsvFile();
        
        Account delAcc=[select id,name from Account where name=: 'DeleteAccount'];
        
        GenericImportController gic11 = new GenericImportController();
        gic11.obj='Account';
        gic11.selectaction = 'Delete';
        gic11.csvAsString = 'testFile.csv';
        //Deletion Success scenario
        String testBlobDel = 'Account Name'+','+'Account Number'+','+'Id'+','+'Employees'+'\n';
        testBlobDel += 'DeleteAccount'+','+'42111'+','+delAcc.id+','+'1'+'\n';
        gic11.csvFileBody=Blob.valueOf(testBlobDel);
        gic11.readcsvFile();
        
        GenericImportController gic12 = new GenericImportController();
        gic12.obj='Account';
        gic12.selectaction = 'Delete';
        gic12.csvAsString = 'testFile.csv';
        //Deletion Success scenario
        String testBlobDel2 = 'Id'+'\n';
        testBlobDel2 += delAcc.id+'\n';
        gic12.csvFileBody=Blob.valueOf(testBlobDel2);
        gic12.readcsvFile();
        
        //As there are no std. date and date-time fields in Account obj, to cover those lines, creating a contact record..      
        GenericImportController gic13 = new GenericImportController();
        gic13.obj = 'Contact';
        gic13.selectaction = 'Insert';
        gic13.csvAsString = 'testFile.csv';
        //Successful Insertion (Couldnt cover Datetime and boolean blocks..)
        //Datetime dt=datetime.newInstance(2016, 4, 06, 12, 00, 0);
        Boolean t= True;        
        String testBlobCt = 'Last Name'+','+'Birthdate'+','+'Email Opt Out'+'\n';
        testBlobCt += 'New Contact'+','+4/6/2016+','+t+'\n';
        gic13.csvFileBody=Blob.valueOf(testBlobCt);        
        gic13.readcsvFile();
        
        GenericImportController gic14 = new GenericImportController();
        gic14.obj = 'Account';
        gic14.selectaction = 'Insert';
        gic14.csvAsString = 'testFile.csv';
        //Successful Insertion
        String testBlob14 = 'Account Name'+','+'Account Number'+'\n';
        testBlob14 += 'VAccount1'+','+'123'+'\n';
        testBlob14 += 'VAccount2'+','+'123'+'\n';    
        gic14.csvFileBody=Blob.valueOf(testBlob14);        
        gic14.readcsvFile();
        
        List<Account> insertedAccs14 = [select id,name from Account where name in : accnames limit 2];
        system.assertEquals(2, insertedAccs14.size());        
        Id ac1= insertedAccs14[0].id;
        Id ac2= insertedAccs14[1].id;
        delete insertedAccs14[0];
        
        GenericImportController gic15 = new GenericImportController();
        gic15.obj = 'Account';
        gic15.selectaction = 'Delete';
        gic15.csvAsString = 'testFile.csv';
        //Partial Deletion..
        String testBlob15 = 'Id'+'\n';
        testBlob15 += ac1+'\n';
        testBlob15 += ac2+'\n';    
        gic15.csvFileBody=Blob.valueOf(testBlob15);        
        gic15.readcsvFile();
        
        GenericImportController gic16 = new GenericImportController();
        gic16.obj='Account';
        gic16.selectaction = 'Update';
        gic16.csvAsString = 'testFile.csv';
        //partial update
        String testBlob16 = 'Account Name'+','+'Account Number'+','+'Id'+','+'Employees'+'\n';
        testBlob16 += 'TestAccount'+','+'42111'+','+insertedAccs3[0].id+','+'1'+'\n';
        testBlob16 += 'TestAccount1'+','+'24212'+','+insertedAccs3[1].id+','+'1'+'\n';  
        testBlob16 += 'TestAccount2'+','+'6723'+','+' '+','+'1'+'\n';
        gic16.csvFileBody=Blob.valueOf(testBlob16);
        gic16.readcsvFile(); 
        
        //To cover catch -not giving 'Id' during update call..
        GenericImportController gicX = new GenericImportController();
        gicX.obj='Account';
        gicX.selectaction = 'Update';
        gicX.csvAsString = 'testFile.csv';
        //Updation Failed
        String testBlobXXX = 'Account Name'+','+'Account Number'+','+'Id'+','+'Employees'+'\n';
        testBlobXXX += ' Test'+','+'42111'+','+','+'1'+'\n';
        testBlobXXX += ' '+','+'24212'+','+','+'1'+'\n';    
        testBlobXXX += ' '+','+'67233'+','+','+'1'+'\n';
        gicX.csvFileBody=Blob.valueOf(testBlobXXX);
        gicX.readcsvFile();
        Test.stopTest();
    }    
}
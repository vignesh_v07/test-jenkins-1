public class subProcessCreation
{
    @InvocableMethod
    public static void subProject(List<Id> projectIds)
    {
        Boolean check = false;
        Integer count = 0;
        system.debug('what came as list?!!!!'+projectIds);
        List<Sub_Project__c> subProjectRecords=[select id,name,Project__c from Sub_Project__c order by createdDate desc limit 50000];
        
        if(subProjectRecords.size() > 0)
        {
            system.debug('inside if!!!!!!total sub proj - '+subProjectRecords.size());
            for(Sub_Project__c allSubPjts: subProjectRecords)
            {
                if(count != 0)
                {
                    system.debug('does project associated to sub project before??!!!!!!'+(allSubPjts.Project__c == projectIds[0]));
                    if(allSubPjts.Project__c == projectIds[0])
                    {                    
                        check = true;                    
                    }
                }
                count++;
            }
            system.debug('after for!!!!check - '+check);
            if(check == false)
            {        
                subProjectRecords[0].Total_SubProject_Count__c = subProjectRecords.size()+1;
                update subProjectRecords;
            }
        }
    }
}
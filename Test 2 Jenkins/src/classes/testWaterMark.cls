public class testWaterMark{

    public string fileName{get;set;}
    public Blob contentFile{get;set;}
    public string attId{get;set;}
    public String prtId{get;set;}
    public boolean pb{get;set;}
    public Attachment attach{get;set;}
    public Account acc {get;set;}
    
    public testWaterMark()
    {
        acc= new Account();
        attach = new Attachment();
        pb=false;
    }
    
    public void submit()
    {        
        acc.Name = 'test';
        insert acc;
        prtId=acc.Id;
        system.debug('parent ID!!!!'+prtId);
        
        attach.name=fileName;
        attach.body=contentFile;
        attach.parentId=prtId;
        insert attach;
        attId=attach.Id;
        system.debug('attach ID!!!!'+attId);
        pb=true;
    }
}
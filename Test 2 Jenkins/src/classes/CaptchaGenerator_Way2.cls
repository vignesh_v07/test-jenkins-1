public class CaptchaGenerator_Way2
{
 public List<Integer> characters;
 public String char1 {get;set;}
 public String char2 {get;set;}
 public String char3 {get;set;}
 public String char4 {get;set;}
 public String char5 {get;set;}
 public String char6 {get;set;}
 
 public String charStr1new {get;set;}
 public String charStr2new {get;set;}
 public String charStr3new {get;set;}
 public String charStr4new {get;set;}
 public String charStr5new {get;set;}
 public String charStr6new {get;set;}
 
 public LIST<Attachment> finalStr1{get;set;}
 public LIST<Attachment> finalStr2{get;set;}
 public LIST<Attachment> finalStr3{get;set;}
 public LIST<Attachment> finalStr4{get;set;}
 public LIST<Attachment> finalStr5{get;set;}
 public LIST<Attachment> finalStr6{get;set;}
 
 public Integer str1 {get;set;} 
 public Integer str2 {get;set;}
 public Integer str3 {get;set;}
 public Integer str4{get;set;} 
 public Integer str5 {get;set;}
 public Integer str6{get;set;} 

 public  Map<Integer, String>  captchaMapNew {get;set;}
 public String Code{get;set;}
 
 public List<StaticResource> imagesfromStaticResource {get;set;}
 
 //Default Constructor
 public CaptchaGenerator_Way2()
 {
     imagesfromStaticResource = new List<StaticResource>();
     characters=new List<Integer>{1,2,3,4,5,6,7};
     //,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62
     getCaptchaList();
 }

    public Integer getstr1(){
        str1=characters[randomNumber()];
        return str1;
    }
    public Integer getstr2(){
        str2=characters[randomNumber()];
        return str2;
    }
    public Integer getstr3(){
        str3 = characters[randomNumber()];
        return str3;
    }
    public Integer getstr4(){
       str4= characters[randomNumber()];
        return str4;
    }
    public Integer getstr5(){
        str5 = characters[randomNumber()];
        return str5;
    }
    public Integer getstr6(){
        str6=characters[randomNumber()];
        return str6;
    }
    //Random Number generator.
    public Integer randomNumber(){
        Integer random = Math.Round(Math.Random() * characters.Size());
        System.debug('@@@@@ Random '+random +' Character '+characters.Size());
        if(random == characters.size()){
            random--;
        }
        return random;
    }

 public void getCaptchaList(){ 
 captchaMapNew=new  Map<Integer, String>();
 captchaMapNew.put(1,'A');
 captchaMapNew.put(2,'B');
 captchaMapNew.put(3,'C');
 captchaMapNew.put(4,'D');
 captchaMapNew.put(5,'E');
 captchaMapNew.put(6,'F');
 captchaMapNew.put(7,'G');
/* captchaMapNew.put(8,'H');
 captchaMapNew.put(9,'I');
 captchaMapNew.put(10,'J');
 captchaMapNew.put(11,'K');
 captchaMapNew.put(12,'L');
 captchaMapNew.put(13,'M');
 captchaMapNew.put(14,'N');
 captchaMapNew.put(15,'O');
 captchaMapNew.put(16,'P');
 captchaMapNew.put(17,'Q');
 captchaMapNew.put(18,'R');
 captchaMapNew.put(19,'S');
 captchaMapNew.put(20,'T');
 captchaMapNew.put(21,'U');
 captchaMapNew.put(22,'V');
 captchaMapNew.put(23,'W');
 captchaMapNew.put(24,'X');
 captchaMapNew.put(25,'Y');
 captchaMapNew.put(26,'Z');
 captchaMapNew.put(27,'X1a');
 captchaMapNew.put(28,'X1b');
 captchaMapNew.put(29,'X1c');
 captchaMapNew.put(30,'X1d');
 captchaMapNew.put(31,'X1e');
 captchaMapNew.put(32,'X1f');
 captchaMapNew.put(33,'X1g');
 captchaMapNew.put(34,'X1h');
 captchaMapNew.put(35,'X1i');
 captchaMapNew.put(36,'X1j');
 captchaMapNew.put(37,'X1k');
 captchaMapNew.put(38,'X1l');
 captchaMapNew.put(39,'X1m');
 captchaMapNew.put(40,'X1n');
 captchaMapNew.put(41,'X1o');
 captchaMapNew.put(42,'X1p');
 captchaMapNew.put(43,'X1q');
 captchaMapNew.put(44,'X1r');
 captchaMapNew.put(45,'X1s');
 captchaMapNew.put(46,'X1t');
 captchaMapNew.put(47,'X1u');
 captchaMapNew.put(48,'X1v');
 captchaMapNew.put(49,'X1w');
 captchaMapNew.put(50,'X1x');
 captchaMapNew.put(51,'X1y');
 captchaMapNew.put(52,'X1z');
 captchaMapNew.put(53,'X11');
 captchaMapNew.put(54,'X12');
 captchaMapNew.put(55,'X13');
 captchaMapNew.put(56,'X14');
 captchaMapNew.put(57,'X15');
 captchaMapNew.put(58,'X16');
 captchaMapNew.put(59,'X17');
 captchaMapNew.put(60,'X18');
 captchaMapNew.put(61,'X19');
 captchaMapNew.put(62,'X10');
*/ 
 
  charStr1new=captchaMapNew.get(getstr1());
  charStr2new=captchaMapNew.get(getstr2()); 
  charStr3new=captchaMapNew.get(getstr3());
  charStr4new=captchaMapNew.get(getstr4());
  charStr5new=captchaMapNew.get(getstr5());
  charStr6new=captchaMapNew.get(getstr6());
  
  StaticResource imageRec;
  
  imageRec = [select id,name,body,NamespacePrefix from staticresource where name=: charStr1new];
  system.debug('imageRec!!!!!'+imageRec.NamespacePrefix);
  imagesfromStaticResource.add(imageRec);
  
  imageRec = [select id,name,body from staticresource where name=: charStr2new];
  imagesfromStaticResource.add(imageRec);
  
  imageRec = [select id,name,body from staticresource where name=: charStr3new];
  imagesfromStaticResource.add(imageRec);
  
  imageRec = [select id,name,body from staticresource where name=: charStr4new];
  imagesfromStaticResource.add(imageRec);
  
  imageRec = [select id,name,body from staticresource where name=: charStr5new];
  imagesfromStaticResource.add(imageRec);
  
  imageRec = [select id,name,body from staticresource where name=: charStr6new];
  imagesfromStaticResource.add(imageRec);  
  Code = null;
  }

public boolean submitCaptcha1(String code)
{
    code=code;
    if(code.length()==6)
    {
        char1=code.subString(0,1);
        char2=code.subString(1,2);
        char3=code.subString(2,3);
        char4=code.subString(3,4);
        char5=code.subString(4,5);
        char6=code.subString(5,6);
        String list1=charStr1New.removeStart('X1');
        list1=list1.removeend('.jpg');
        String list2=charStr2New.removeStart('X1');
        list2=list2.removeend('.jpg');
        String list3=charStr3New.removeStart('X1');
        list3=list3.removeend('.jpg');
        String list4=charStr4New.removeStart('X1');
        list4=list4.removeend('.jpg');
        String list5=charStr5New.removeStart('X1');
        list5=list5.removeend('.jpg');
        String list6=charStr6New.removeStart('X1');
        list6=list6.removeend('.jpg');
        if(char1.equals(list1) && char2.equals(list2) && char3.equals(list3) && char4.equals(list4) && char5.equals(list5) && char6.equals(list6))
        {
            //ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.INFO,'Captcha entered is correct!');
            //ApexPages.addMessage(mess);
            return true;
        }
        else
        {
            ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,'Please check the captcha entered!');
            ApexPages.addMessage(mess);
            return false;
        }
    }
    else
    {
        ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,'Please enter valid captcha code.');
        ApexPages.addMessage(mess);
        return false;
    }
}
}
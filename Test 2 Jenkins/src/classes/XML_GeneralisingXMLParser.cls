public class XML_GeneralisingXMLParser {

    public List<Sobject> listtobereturned=new List<Sobject>();
    public Integer nodeCount=0;
    public Date datevalues{get;set;}
    public DateTime datetimevalues{get;set;}
    public Sobject a;
    public String sobjectName;
    public Map<String,String> customSettingMap=new Map<String,String>();
    public Map<String,String> customsettingMapDTValue=new Map<String,String>();
    public sObject obj;
       
    //Default Constructor..
    public XML_GeneralisingXMLParser() {}
  
    // DOM PARSER...
    public List<Sobject> parse(String parsedText,String Objectname,Map<String,Sobject> customSettingMap1) 
    {
        sobjectName=Objectname;
        system.debug('object name!!!!!!!!'+sobjectName); 
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(sobjectName);
        obj = targetType.newSObject();
        
        for(Sobject customSettingvalue: customSettingMap1.values())
        {
            customSettingMap.put(String.valueOf(customSettingvalue.get('Name')),String.valueOf(customSettingvalue.get('RespectiveAPIName__c')));
            customsettingMapDTValue.put(String.valueOf(customSettingvalue.get('Name')),String.valueOf(customSettingvalue.get('DataType__c')));
        }
        
        DOM.Document doc = new DOM.Document();               
        doc.load(parsedText);
        DOM.XMLNode root = doc.getRootElement();
        walkThrough(root); 
        // Adding the mapped fields(a) to the list of Sobject..
        listtobereturned.add(obj);
        system.debug('list to be returned!!!!!!!'+listtobereturned);       
        return listtobereturned;   
    }
    private void walkThrough(DOM.XMLNode node) 
    {
      String result = '\n';
      String nameWithSpace='';
      Boolean num;
      boolean email;
      boolean dat;
      boolean cBox;
      boolean datime;
      String finalMutiPicklistValues='';
      nodeCount++;
      system.debug('node type!!!!!!'+node.getNodeType());
      if (node.getNodeType() == DOM.XMLNodeType.COMMENT) {
        //return 'Comment (' +  node.getText() + ')';
      }
      if (node.getNodeType() == DOM.XMLNodeType.TEXT) {    
        //return 'Text (' + node.getText() + ')';
      }
      if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
        String recordtypeId='recordtypeId';
        result += 'Element: ' + node.getName();
        system.debug('node name!!!!!!'+node.getName());        
            if(nodeCount==1)
            { 
                if(node.getname().contains('-'))
                {                           
                    nameWithSpace=node.getname().replaceAll('-',' ');
                    obj.put(recordtypeId,nameWithSpace);
                }
                else 
                {                                                 
                    obj.put(recordtypeId,node.getname());
                }
            }
        }
        String datatype=customsettingMapDTValue.get(node.getName());
        system.debug('data type!!!!'+datatype);        
        if(datatype=='Number' || datatype=='Percent' || datatype == 'Currency')
        {
            num=true;
        }
        else if(datatype=='Date')
        {
            dat=true;
        }        
        else if(datatype=='email')
        {
            email=true;
        }
        else if(datatype=='Checkbox')
        {
            cBox=true;
        }  
        else if(datatype=='Date/Time')
        {
            datime=true;
        }   
        //my code starts..
        /*List<String> childlist = new List<String>();
        if(!(node.getChildElements()==null))
        { 
            String nodenamehavingtextYes='';
            String valueforNonMultiPicklistnode='';       
            for (Dom.XMLNode innerchild: node.getChildElements()) {                                
                   if(innerchild.gettext().equals('Yes')){
                       if((innerchild.getname().contains('-')) || ((innerchild.getname().contains('_'))))
                       {                           
                           nameWithSpace=innerchild.getname().replaceAll('-',' ');
                           nameWithSpace=nameWithSpace.replaceAll('_','');
                           childlist.add(nameWithSpace); 
                       } 
                       else{
                           nodenamehavingtextYes= innerchild.getname();
                           valueforNonMultiPicklistnode= innerchild.gettext();
                           childlist.add(innerchild.getname());
                       }                      
                    }                          
             }

             if(childList.size()>0)
             {                          
                 finalMutiPicklistValues=methodconcatenate(childlist); 
                 try
                 {
                     obj.put(customSettingMap.get(node.getName()),finalMutiPicklistValues);           
                 }
                 catch(Exception e)
                 {
                     obj.put(customSettingMap.get(nodenamehavingtextYes),valueforNonMultiPicklistnode);
                 }
             }
         }       
        */
        if (node.getText().trim() != '') {
          result += ', text=' + node.getText().trim();
            try
            {
                //new 6/12/2014.....
                if(dat==true)
                {
                    //Date datevalues=Date.parse(node.getText().trim());
                    datevalues=DateParser.parseDate(node.getText().trim());
                    obj.put(customSettingMap.get(node.getName()),datevalues);
                }
                // contains a dot and numeric...                
                else if(node.getText().trim().contains('.'))
                {
                    // new 6/12/2014......
                    if(num==true)
                    {
                        String ex='\''+node.getText()+'\'';
                        system.debug('num?!!!!!!!!'+ex);
                        Decimal value=decimal.valueOf(ex.replaceAll('\'',''));
                        obj.put(customSettingMap.get(node.getName()),value);               
                    }
                    else if(email==true)
                    {
                        obj.put(customSettingMap.get(node.getName()),node.getText().trim());                        
                    }
                    else
                    {
                        String ex='\''+node.getText()+'\'';
                        obj.put(customSettingMap.get(node.getName()),ex.replaceAll('\'',''));
                    }
                }
                // For Check box fields..
                else if(cBox==true)
                {
                    if(node.getText().trim().equalsIgnorecase('Yes') || node.getText().trim().equalsIgnorecase('true'))
                    {
                        if(node.getText().trim().equalsIgnorecase('Yes'))
                        {                            
                            String yesTotrue = 'true';
                            Boolean val=Boolean.valueOf(yesTotrue);
                            obj.put(customSettingMap.get(node.getName()),val);
                        }
                        else
                        {
                            Boolean val=Boolean.valueOf(node.getText().trim());
                            obj.put(customSettingMap.get(node.getName()),val);                        
                        }
                    }
                }
                else if(datime == true)
                {
                    datetimevalues=DateParser.parseDateTime(node.getText().trim());
                    obj.put(customSettingMap.get(node.getName()),datetimevalues);
                }
                else
                {
                    if(num==true)
                    {
                        Decimal value=decimal.valueOf(node.getText());
                        system.debug('num?!!!!!!!!'+value);
                        obj.put(customSettingMap.get(node.getName()),value);
                    }
                    else
                    {   
                        obj.put(customSettingMap.get(node.getName()),node.getText().trim());          
                    }
                }
            }
            catch(Exception e)
            {
                ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage());
                //ApexPages.addMessage(mess);
            }
        }
        system.debug('obj value!!!!!!!!!!!!'+obj);
        //Iterating through each node...
        for (Dom.XMLNode child: node.getChildElements()) { 
            DOM.XMLNode value=child;
            walkThrough(child);
        }            
     // return '';  //should never reach here
    }
    
    // This method will concatenate strings using semi-colon for multi-picklist fields..
    public String methodconcatenate(List<String> listValues)
    {
        String a1='';
        Integer b=listValues.size();
         if(listValues.size()>0)
         {
             for(String s :listValues){
               a1+=s+';';                               
             }
             // remove last additional comma from string
             if(!(a1==null))
             {
                 a1= a1.subString(0,a1.length()-1);                                
             }
         }
         return a1;
    }
}
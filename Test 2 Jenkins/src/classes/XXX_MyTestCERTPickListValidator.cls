public class XXX_MyTestCERTPickListValidator
{
  //return type for the validation, includes boolean value for the validation, along with error message
  public class CheckValidPickListValueResult
  {
    public boolean isValidValue{get;set;}
    public String  errorMessage{get;set;}
  }
  //gets list of picklist Values for the object name and the field Name
  public Static List<String> getPicklistValues(Sobject objectApiame,String fieldAPIName){ 
  string objname=String.valueOf(objectApiame);
  system.debug('object api name!!!!!!!!!'+objectApiame);
  system.debug('field api name!!!!!!!!!!'+fieldAPIName);
  List<String> pickListvalues=new List<String>();
  Schema.SObjectType objectType = Schema.getGlobalDescribe().get(objname);
    system.debug('Schema.SObjectType!!!!!!!!!!'+objectType);
   Sobject objectNameobj = objectType.newSObject();
   system.debug('Sobject!!!!!!!!!!'+objectNameobj);
   Schema.sObjectType sobjecttypeobj = objectNameobj.getSObjectType(); 
   system.debug('Schema.sObjectType!!!!!!!!!!'+sobjecttypeobj);   
    Schema.DescribeSObjectResult describeObject = sobjecttypeobj.getDescribe();
       system.debug('Schema.DescribeSObjectResult!!!!!!!!!!'+describeObject); 
   // DescribeLayoutResult....................
    //DescribeLayoutResult result = binding.describeLayout(objectApiame,rectypeId); 
    //PicklistEntry[] values = result.recordTypeMappings[0].picklistsForRecordType[12345].picklistValues;
    
    Map<String, Schema.SObjectField> fieldMap = describeObject .fields.getMap();
   system.debug('Map<String, Schema.SObjectField>!!!!!!!!!!'+fieldMap);   
    List<Schema.PicklistEntry> pickListEntries = fieldMap.get(fieldAPIName).getDescribe().getPickListValues(); 
   system.debug('List<Schema.PicklistEntry>!!!!!!!!!!'+pickListEntries);       
    for (Schema.PicklistEntry pickLValue : pickListEntries )
     { 
      pickListvalues.add(pickLValue.getValue());
   }
    system.debug('final picklist values!!!!!!!!!'+pickListvalues);
  return pickListvalues;
  }
   //validates data for the single select picklist , for the object name and the field Name
  public Static CheckValidPickListValueResult checkValidPicklistValue(Sobject objectApiName,String fieldApiName,String dataValue,String rectypename)
  {
    system.debug('object api name!!!!!!!!!'+objectApiName);
    system.debug('field api name!!!!!!!!!!'+fieldAPIName);
    system.debug('data value!!!!!!!!'+dataValue);
    system.debug('record type name'+rectypename);
    //List<String> values=[select fieldApiName from objectApiName where recordtypeID =: [select id from RecordType where name=: rectypename]];
    CheckValidPickListValueResult pickListResult=new CheckValidPickListValueResult();
    List<String> picklistValues= getPicklistValues(objectApiName,fieldApiName);
    system.debug('picklistValues from common method!!!!!!!!'+picklistValues);
    
    for(String picklistValue : picklistValues)
    {
       system.debug('equal??!!!!!!!!!!!'+(picklistValue.equals(dataValue)));
       if(picklistValue.equals(dataValue))
       {
        pickListResult.isValidValue=true;
        return pickListResult;
       }
    }
    system.debug('after for.. pick list result!!!!!!!!'+pickListResult);
    pickListResult.isValidValue=false;
    pickListResult.errorMessage=dataValue+' is not a valid value for '+fieldApiName+'. Please select from '+picklistValues;
    system.debug('any error??? pick list result!!!!!!!!'+pickListResult);
    return pickListResult;
  }
  
  //validates data for the multi select picklist , for the object name and the field Name
  public Static CheckValidPickListValueResult checkValidMultiPicklistValue(SObject objectApiName,String fieldApiName,String dataValue)
  {
    system.debug('object api name!!!!!!!!!'+objectApiName);
    system.debug('field api name!!!!!!!!!!'+fieldAPIName);
    system.debug('data value!!!!!!!!'+dataValue);
    CheckValidPickListValueResult pickListResult=new CheckValidPickListValueResult();
    List<String> picklistValues= getPicklistValues(objectApiName,fieldApiName);
    system.debug('picklistValues from common method!!!!!!!!'+picklistValues);    
    List<String> selectedValues =dataValue.split(';');
    system.debug('splitted multi picklist values!!!!!'+selectedValues);
    String inValidValues='';
    for(String singleValue:selectedValues )
     {
        system.debug('value from outer loop!!!!!!!!'+singleValue);
        Boolean isInvalidvalue=true;
        for(String picklistValue : picklistValues)
        {
          system.debug('from inner loop... equal??!!!!!!!!'+picklistValue+'!!!'+(picklistValue.equals(singleValue)));
           if(picklistValue.equals(singleValue))
           {
            isInvalidvalue=false;
            break;
           }     
        }
        if(isInvalidValue==true)
         {
           inValidValues=inValidValues+singleValue+',';
           pickListResult.isValidValue=false;
         }
    }
    system.debug('pick list result!!!!!!!!!!'+pickListResult);
    if(pickListResult.isValidValue==false)
    {
        pickListResult.errorMessage=inValidValues+'is/are not valid values for '+fieldApiName+'. Please select from '+picklistValues;
        system.debug('inside if in line 94!!!!!!!!!!'+pickListResult);
        return pickListResult;    
    }        
    pickListResult.isValidValue=true;
    return pickListResult;
  }

}
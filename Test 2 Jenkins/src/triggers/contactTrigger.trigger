trigger contactTrigger on Contact (before insert,before update) 
{
    Map<string, contact> conRecs = new Map<String, contact>();
   
    if((trigger.isBefore && trigger.isInsert)||(trigger.isBefore && trigger.isUpdate))
    {
        for(Contact con: Trigger.new)
        {
            conRecs.put(con.id, con);
        }
        
        if(conRecs.size()> 0)
        {
            for(Contact c : conRecs.values())
            {
                if(c.status__c == 'Approved')
                {
                    c.Dup_Level__c  = c.Level__c;
                    c.Dup_Description__c = c.Description__c;
                }
                else if (c.status__c == 'Rejected')
                {
                    if(c.Dup_Description__c != null)
                    {
                        c.Description__c = c.Dup_Description__c;
                    }
                    if(c.Dup_Level__c != null)
                    {
                        c.Level__c = c.Dup_Level__c;
                    }
                }
            }
        }        
    }
}
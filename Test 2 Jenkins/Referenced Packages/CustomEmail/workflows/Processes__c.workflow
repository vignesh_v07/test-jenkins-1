<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>GCS_LanguageUpdateSpanish</fullName>
        <field>Language_Backend__c</field>
        <literalValue>es</literalValue>
        <name>GCS_LanguageUpdateSpanish</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GCS_UpdateLanguageEnglish</fullName>
        <field>Language_Backend__c</field>
        <literalValue>en</literalValue>
        <name>GCS_UpdateLanguageEnglish</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GCS_LanguageUpdateEnglish</fullName>
        <actions>
            <name>GCS_UpdateLanguageEnglish</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Processes__c.Language__c</field>
            <operation>equals</operation>
            <value>English</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GCS_LanguageUpdateSpanish</fullName>
        <actions>
            <name>GCS_LanguageUpdateSpanish</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Processes__c.Language__c</field>
            <operation>equals</operation>
            <value>Spanish</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
